<?php
  
function active_class($path, $active = 'active') {
  return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

function show_class($path) {
    return call_user_func_array('Request::is', (array)$path) ? 'show' : '';
}

function collapsed_class($path) {
    return call_user_func_array('Request::is', (array)$path) ? '' : 'collapsed';
}