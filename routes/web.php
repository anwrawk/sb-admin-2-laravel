<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('buttons', function () { return view('buttons'); })->name('buttons');
Route::get('cards', function () { return view('cards'); })->name('cards');
Route::get('utilities-color', function () { return view('utilities-color'); })->name('utilities-color');
Route::get('utilities-border', function() { return view('utilities-border'); })->name('utilities-border');
Route::get('utilities-animation', function() { return view('utilities-animation'); })->name('utilities-animation');
Route::get('utilities-other', function() { return view('utilities-other'); })->name('utilities-other');
Route::get('login', function() { return view('login'); })->name('login');
Route::get('register', function() { return view('register'); })->name('register');
Route::get('forgot-password', function() { return view('forgot-password'); })->name('forgot-password');
Route::get('blank', function() { return view('blank'); })->name('blank');
Route::get('charts', function() { return view('chart'); })->name('charts');
Route::get('tables', function() { return view('table'); })->name('tables');